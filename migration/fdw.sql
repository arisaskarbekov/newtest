CREATE EXTENSION postgres_fdw;

grant usage on FOREIGN DATA WRAPPER postgres_fdw to newtest;

CREATE SERVER other_db_server
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host '127.0.0.1', dbname 'other_db', port '5432');

GRANT USAGE ON FOREIGN SERVER other_db_server TO newtest;

CREATE USER MAPPING FOR newtest
SERVER other_db_server
OPTIONS (user 'newtest', password 'newtest');