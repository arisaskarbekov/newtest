package main

import (
	"testing"
	"net/http"
	"net/http/httptest"
)

func TestWork(t *testing.T) {
	request, _ := http.NewRequest("GET", "/work", nil)
	response := httptest.NewRecorder()

	work(response, request)

	got := response.Body.String()
	want := "Петя работает\n"

	if got != want {
		t.Errorf("got %v, want %v", got, want)
	}
}

func TestSalary(t *testing.T) {
	request, _ := http.NewRequest("GET", "/salary", nil)
	response := httptest.NewRecorder()

	salary(response, request)

	got := response.Body.String()
	want := "Петя получает ЗП\n"

	if got != want {
		t.Errorf("got %v, want %v", got, want)
	}
}