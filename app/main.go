package main

import (
  "fmt"
  "net/http"
)

func work(w http.ResponseWriter, req *http.Request) {

  msg := "Петя работает\n"
  fmt.Println(msg)
  fmt.Fprintf(w, msg)
}

func salary(w http.ResponseWriter, req *http.Request) {

  msg := "Петя получает ЗП\n"
  fmt.Println(msg)
  fmt.Fprintf(w, msg)
}

func main() {

  http.HandleFunc("/work", work)
  http.HandleFunc("/salary", salary)

  http.ListenAndServe(":8090", nil)
}