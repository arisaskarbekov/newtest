#!/bin/bash

if [ "$1" == "" ] || [ "$2" == "" ] || [ "$3" == "" ] || [ "$4" == "" ] || [ "$5" == "" ]
then
        echo "'\$1', '\$2', '\$3', '\$4', '\$5' parameters should be provided"
        exit
fi

IS_CONTAINER=$1 # 0 - false, 1 - true
BACKUP_PATH=$2
DB_NAME=$3
DB_USER=$4
CONTAINER_NAME=$5

if [ "$IS_CONTAINER" -eq 1 ]
then
        /usr/bin/docker exec $CONTAINER_NAME /usr/bin/pg_dump -U $DB_USER $DB_NAME | /usr/bin/gzip > $BACKUP_PATH/$DB_NAME-`date '+%F'`.gz
else
        /usr/bin/pg_dump -U $DB_USER $DB_NAME | /usr/bin/gzip > $BACKUP_PATH/$DB_NAME.`date '+%F'`.gz
fi